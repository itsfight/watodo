class Suggestion < ActiveRecord::Base
  has_many :posts
  has_many :provider_suggestions
  belongs_to :crowd_range
  belongs_to :category_range
  belongs_to :duration_range
  belongs_to :money_range
  belongs_to :site_range
  accepts_nested_attributes_for :posts, :provider_suggestions, reject_if: :all_blank
end
