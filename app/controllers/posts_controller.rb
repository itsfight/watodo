class PostsController < ApplicationController
  def new
    @post = Post.new
  end

  def create
    post = Post.new(post_paramz)
    if post.save
      redirect_to controller: 'suggestions', action: :show, id: post.suggestion_id
    else
      render :new
    end
  end

  private

  def post_paramz
    params.require(:post).permit(:title, :body, {suggestion: [:title]})
  end
end
