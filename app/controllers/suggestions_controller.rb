class SuggestionsController < ApplicationController

  def filter
    @suggestion_filter = SuggestionFilter.new
  end

  def random
    @suggestion_filter = SuggestionFilter.new(suggestion_filter_params)

    suggestions = Suggestion.where({duration_range_id: @suggestion_filter.duration,
                                    crowd_range_id: @suggestion_filter.crowd,
                                    category_range_id: @suggestion_filter.category,
                                    site_range_id: @suggestion_filter.site,
                                    money_range_id: @suggestion_filter.money}.delete_if {|k,v| v.empty? })

    if suggestions.empty?
      flash[:state] = 'try one more time!'
      redirect_to suggestions_filter_path

    else
      @suggestion = suggestions[Random.rand(suggestions.size)]
    end
  end

  def show
    @suggestion = Suggestion.find(params[:id])
  end

  def new_post
    @suggestion = Suggestion.new
    @suggestion.posts=[Post.new]
    render :new
  end

  def new_provider_suggestion
    @suggestion = Suggestion.new
    @suggestion.provider_suggestions=[ProviderSuggestion.new]
    render :new
  end

  def create
    @suggestion = Suggestion.new(suggestion_params)
    if @suggestion.save
      redirect_to @suggestion
    else
    #   should do something! todo
    end
  end

  private

  def suggestion_params
    params.require(:suggestion).permit(:title, :description, :category_range_id, :crowd_range_id,
                                       :site_range_id, :duration_range_id, :money_range_id,
                                       posts_attributes: [:title, :body],
                                       provider_suggestions_attributes: [:title, :description])
  end

  def suggestion_filter_params
    params.require(:suggestion_filter).permit(:duration, :category, :crowd, :site, :money)
  end

end

class SuggestionFilter
  include ActiveModel::Model
  attr_accessor :duration, :category, :crowd, :site, :money

end