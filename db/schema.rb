# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141215212126) do

  create_table "category_ranges", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "crowd_ranges", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "duration_ranges", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "money_ranges", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "posts", force: true do |t|
    t.string   "title"
    t.integer  "suggestion_id"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  add_index "posts", ["suggestion_id"], name: "index_posts_on_suggestion_id"
  add_index "posts", ["user_id"], name: "index_posts_on_user_id"

  create_table "provider_suggestions", force: true do |t|
    t.string   "title"
    t.integer  "provider_id"
    t.integer  "suggestion_id"
    t.integer  "duration_range_id"
    t.integer  "category_range_id"
    t.integer  "crowd_range_id"
    t.integer  "money_range_id"
    t.integer  "site_range_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
  end

  create_table "providers", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "site_ranges", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "suggestions", force: true do |t|
    t.integer  "duration_range_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category_range_id"
    t.integer  "crowd_range_id"
    t.integer  "site_range_id"
    t.integer  "money_range_id"
    t.string   "title"
    t.text     "description"
  end

  add_index "suggestions", ["category_range_id"], name: "index_suggestions_on_category_range_id"
  add_index "suggestions", ["crowd_range_id"], name: "index_suggestions_on_crowd_range_id"
  add_index "suggestions", ["duration_range_id"], name: "index_suggestions_on_duration_range_id"
  add_index "suggestions", ["money_range_id"], name: "index_suggestions_on_money_range_id"
  add_index "suggestions", ["site_range_id"], name: "index_suggestions_on_site_range_id"

  create_table "users", force: true do |t|
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "password_digest"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
