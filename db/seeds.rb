# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
category_range = CategoryRange.create(name: 'sport')
CategoryRange.create(name: 'art')
CategoryRange.create(name: 'beauty')
crowd_range = CrowdRange.create(name: 'alone')
CrowdRange.create(name: 'two of us')
CrowdRange.create(name: 'crowd')
CrowdRange.create(name: 'big crowd')
duration_range = DurationRange.create(name: 'fast')
DurationRange.create(name: 'long')
DurationRange.create(name: 'meh')
money_range = MoneyRange.create(name: 'cheap')
MoneyRange.create(name: 'expensive')
MoneyRange.create(name: 'meh')
site_range = SiteRange.create(name: 'home')
SiteRange.create(name: 'in da city')
SiteRange.create(name: 'suburb')
SiteRange.create(name: 'meh')
suggestion = Suggestion.create(title: 'dance hip-hop!',
                                      category_range_id: category_range.id,
                                      crowd_range_id: crowd_range.id,
                                      duration_range_id: duration_range.id,
                                      money_range_id: money_range.id,
                                      site_range_id: site_range.id)

suggestion.posts.create(title: 'How i learnt hip-hop ;)', body: 'it was not easy, but I did it! bla bla bla...
 So I think that everybody have to try it for sure!!')