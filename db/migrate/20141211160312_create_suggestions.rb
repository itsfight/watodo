class CreateSuggestions < ActiveRecord::Migration
  def change
    create_table :suggestions do |t|
      t.references :duration_range, index: true

      t.timestamps
    end
  end
end
