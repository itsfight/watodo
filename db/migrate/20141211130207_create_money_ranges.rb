class CreateMoneyRanges < ActiveRecord::Migration
  def change
    create_table :money_ranges do |t|
      t.string :name

      t.timestamps
    end
  end
end
