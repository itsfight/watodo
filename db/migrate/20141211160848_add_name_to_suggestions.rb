class AddNameToSuggestions < ActiveRecord::Migration
  def change
    add_column :suggestions, :title, :string
    add_column :suggestions, :description, :text
  end
end
