class CreateCategoryRanges < ActiveRecord::Migration
  def change
    create_table :category_ranges do |t|
      t.string :name

      t.timestamps
    end
  end
end
