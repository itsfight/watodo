class AddRefsToSuggestion < ActiveRecord::Migration
  def change
    add_reference :suggestions, :crowd_range, index: true
    add_reference :suggestions, :site_range, index: true
    add_reference :suggestions, :money_range, index: true
  end
end
