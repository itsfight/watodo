class CreateProviderSuggestions < ActiveRecord::Migration
  def change
    create_table :provider_suggestions do |t|
      t.string :title
      t.references :provider
      t.references :suggestion
      t.references :duration_range
      t.references :category_range
      t.references :crowd_range
      t.references :money_range
      t.references :site_range

      t.timestamps
    end
  end
end
