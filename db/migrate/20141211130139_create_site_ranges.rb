class CreateSiteRanges < ActiveRecord::Migration
  def change
    create_table :site_ranges do |t|
      t.string :name

      t.timestamps
    end
  end
end
