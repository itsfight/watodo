class CreateCrowdRanges < ActiveRecord::Migration
  def change
    create_table :crowd_ranges do |t|
      t.string :name

      t.timestamps
    end
  end
end
