class AddCategoryRangeRefToSuggestion < ActiveRecord::Migration
  def change
    add_reference :suggestions, :category_range, index: true
  end
end
