class AddDescriptionToProviderSuggestion < ActiveRecord::Migration
  def change
    add_column :provider_suggestions, :description, :text
  end
end
